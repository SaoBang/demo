
<?php
$controllers = array(
  'pages' => ['home', 'error'],
  'posts' => ['index'],
  //'post' => ['index'], // bổ sung thêm
);

if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
  $controller = 'pages';
  $action = 'error';
}else{
  // set mặc định là home page
  $controller = 'home';
}

//include_once('controllers/posts_controller.php');
include_once('controllers/' . $controller . '_controller.php');
$klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
// echo $klass;
$controller = new $klass;
$controller->$action();